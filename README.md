# gandalf_snake


## Install:

### 1) get the code, install where you want (ex $HOME)

cd $HOME
git clone  git@forgemia.inra.fr:bioger/gandalf_snake.git

or 

wget https://forgemia.inra.fr/bioger/gandalf_snake/-/archive/main/gandalf_snake-main.zip

unzip/untar if necessary


### 2) go to your directory (ex /work/project/baric/BIOGER/VISA/variant_calling ) and run

**Now you have 2 solutions:**


a) **You don't want to install the depencies**, all conda packages will be downlaod and install at each run. You just need `snakemake`, so on genotoul load `miniconda`

b) **You don't want to install the depencies at each run.** So create a conda env and init it before run all job.


**ex: run in a) mode**

in your working dir:

```
#!/bin/sh

INSTALL=$HOME/gandalf_snake-main
DATADIR=/work/project/bbdata/data-browser/private/zymoseptoria_epidemio/populations/VISA/raw
REFDIR=/work/project/baric/BIOGER/VISA/variant_calling/data

module load bioinfo/snakemake-5.25.0

for R1 in `ls -f $DATADIR/*R1.fastq.gz`


do
    filename=`basename $R1`
    prefix=${filename:0:13}
    echo $prefix

    mkdir -p ${prefix}
    cd ${prefix}

    cat > ${prefix}.sh <<EOF
#!/bin/sh

#SBATCH -J gandalf_${prefix}
#SBATCH -o ${prefix}.stdout
#SBATCH -e ${prefix}.stderr
#SBATCH --mem=30G
#SBATCH -c 1
###SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --export=ALL


snakemake --snakefile $INSTALL/snakefile --cluster "sbatch -c {threads} --mem=120G" -j 500 --use-conda --config fastq_R1=$DATADIR/${prefix}_R1.fastq.gz fastq_R2=$DATADIR/${prefix}_R2.fastq.gz genome=$REFDIR/Mygr_323_reformat_with_mito.clean.fsa adapters=$INSTALL/data/TruSeq3-PE-2.fa dir=${prefix} ID=${prefix} species=ztritici TE=$REFDIR/TE.bed lowcomplex=$REFDIR/lowcomplex.bed

EOF

    sbatch ${prefix}.sh
    cd ..
done
```


**ex: run in b) mode**


in your working dir:

*create conda env*
```
conda env create -n GANDALF -f $INSTALL/envs/gandalf.yaml
conda activate GANDALF
module load bioinfo/snakemake-5.25.0
```
*run without use conda*
```
#!/bin/sh


INSTALL=$HOME/gandalf_snake-main
DATADIR=/work/project/bbdata/data-browser/private/zymoseptoria_epidemio/populations/VISA/raw
REFDIR=/work/project/baric/BIOGER/VISA/variant_calling/data

for R1 in `ls -f $DATADIR/*R1.fastq.gz`


do
    filename=`basename $R1`
    prefix=${filename:0:13}
    echo $prefix

    mkdir -p ${prefix}
    cd ${prefix}

    cat > ${prefix}.sh <<EOF
#!/bin/sh

#SBATCH -J gandalf_${prefix}
#SBATCH -o ${prefix}.stdout
#SBATCH -e ${prefix}.stderr
#SBATCH --mem=30G
#SBATCH -c 1
###SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --export=ALL


snakemake --snakefile $INSTALL/snakefile --cluster "sbatch -c {threads} --mem=120G" -j 500  --config fastq_R1=$DATADIR/${prefix}_R1.fastq.gz fastq_R2=$DATADIR/${prefix}_R2.fastq.gz genome=$REFDIR/Mygr_323_reformat_with_mito.clean.fsa adapters=$INSTALL/data/TruSeq3-PE-2.fa dir=${prefix} ID=${prefix} species=ztritici TE=$REFDIR/TE.bed lowcomplex=$REFDIR/lowcomplex.bed

EOF

    sbatch ${prefix}.sh
    cd ..
done

```




##################

basic usage:

```
(cluster mode)

snakemake --snakefile $INSTALL/snakefile --cluster "sbatch -c {threads} --mem=120G" -j 500 --use-conda --config fastq_R1=$DATADIR/${prefix}_R1.fastq.gz fastq_R2=$DATADIR/${prefix}_R2.fastq.gz genome=$REFDIR/Mygr_323_reformat_with_mito.clean.fsa adapters=$INSTALL/data/TruSeq3-PE-2.fa dir=${prefix} ID=${prefix} species=ztritici TE=$REFDIR/TE.bed lowcomplex=$REFDIR/lowcomplex.bed
```

inputs:
* read (R1, R2)
* genome (reference)
* adapters (for trimming)
* dir (working directory)
* ID (ID, sample)
* species
* TE in bed file
* lowcomplexity in bed file




