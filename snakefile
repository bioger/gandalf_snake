# workflow inspired from https://github.com/nlapalu/misc/blob/master/GandalfWorkflow.sh
# developped in the frame of the GANDALF project
# https://anr.fr/Projet-ANR-12-ADAP-0009


# config parameters
R1 = config['fastq_R1']
R2 = config['fastq_R2']
ID = config['ID']
species = config['species']
genome = config['genome']
adapters = config['adapters']
TE = config['TE']
lowcomplex = config['lowcomplex']


if "dir" not in config:
    raise Exception("Missing result directory with dir parameter")
result_dir = os.path.abspath(config['dir'])

# Trimming
trim_dir = "{}/trimmomatic".format(result_dir)
# Mapping
map_dir = "{}/bwa".format(result_dir) 
index_dir = "{}/bwa/index".format(result_dir) 
# Picard
picard_dir = "{}/picard".format(result_dir)
# Samtools
samtools_dir = "{}/samtools".format(result_dir)
# Freebayes
freebayes_dir = "{}/freebayes".format(result_dir)
# VCFFiltering
filter_dir = "{}/VCFFiltering".format(result_dir)
 

rule all:
    input:
        ## Trimming ##
#        "{}/R1.paired.gz".format(trim_dir),
#        "{}/R2.paired.gz".format(trim_dir),
        ## Indexing
#        "{}/{}".format(index_dir,os.path.basename(genome)),
#        "{}/{}.amb".format(index_dir,os.path.basename(genome)),
#        "{}/{}.ann".format(index_dir,os.path.basename(genome)),
#        "{}/{}.bwt".format(index_dir,os.path.basename(genome)),
#        "{}/{}.pac".format(index_dir,os.path.basename(genome)),
#        "{}/{}.sa".format(index_dir,os.path.basename(genome)),
        ## Mapping ##
#        "{}/{}.sorted.bam".format(map_dir,ID),
        ## ReadGroup ##
#        "{}/RG.{}.sorted.bam".format(picard_dir,ID),
        ## Rmdup ##
#        "{}/dup.{}.sorted.bam".format(samtools_dir,ID),
        ## Clean Mapping ##
#        "{}/min30.{}.sorted.bam".format(samtools_dir,ID),
        ## Unique list #
#        "{}/min30.{}.lst".format(samtools_dir,ID),
        ## Paired bam ##
#        "{}/paired.min30.{}.sorted.bam".format(picard_dir,ID)
        ## Freebayes ##
#        "{}/min30.mono.{}.vcf".format(freebayes_dir,ID)
#       ## VCFFiltering
#        "{}/{}.filter.vcf".format(filter_dir,ID)
        ## compress 
#        "{}/{}.filter.vcf.gz".format(filter_dir,ID)
        ## tabix
        "{}/{}.filter.vcf.gz.tbi".format(filter_dir,ID)


## Trimming ##
rule gandalf_trimming:
    input:
        R1 = R1,
        R2 = R2
    output:
        R1_paired = "{}/R1.paired.gz".format(trim_dir),
        R1_unpaired = "{}/R1.unpaired.gz".format(trim_dir),
        R2_paired = "{}/R2.paired.gz".format(trim_dir),
        R2_unpaired = "{}/R2.unpaired.gz".format(trim_dir)
    params:
        adapters = adapters
    threads: 8
    conda:
        "envs/gandalf.yaml"
    shell:
        "trimmomatic PE -threads {threads} -phred33 {input.R1} {input.R2} {output.R1_paired} {output.R1_unpaired} {output.R2_paired} {output.R2_unpaired} ILLUMINACLIP:{params.adapters}:2:30:12:1:true SLIDINGWINDOW:4:20 MINLEN:16"

## Indexing ##
rule gandalf_indexing:
    input:
        genome = genome,
    output:
        index_dir = directory(index_dir),
        genome = "{}/{}".format(index_dir,os.path.basename(genome)),
        idx1 = "{}/{}.amb".format(index_dir,os.path.basename(genome)),
        idx2 = "{}/{}.ann".format(index_dir,os.path.basename(genome)),
        idx3 = "{}/{}.bwt".format(index_dir,os.path.basename(genome)),
        idx4 = "{}/{}.pac".format(index_dir,os.path.basename(genome)),
        idx5 = "{}/{}.sa".format(index_dir,os.path.basename(genome))
    conda:
        "envs/gandalf.yaml"
    shell:
        "cp {input.genome} {output.index_dir} && bwa index {output.genome}"

## Mapping ##
rule gandalf_mapping:
    input:
        R1 = "{}/R1.paired.gz".format(trim_dir),
        R2 = "{}/R2.paired.gz".format(trim_dir),
        genome = "{}/{}".format(index_dir,os.path.basename(genome)),
    output:
        bam = "{}/{}.sorted.bam".format(map_dir,ID)
    threads: 8
    conda:
        "envs/gandalf.yaml",
    shell:
        "bwa mem -t {threads} -M {input.genome} {input.R1} {input.R2} | samtools view -bS - | samtools sort -m 1G -o {output.bam} -"

## Add Group ##
rule gandalf_addRG:
    input:
        bam = "{}/{}.sorted.bam".format(map_dir,ID)
    output:
        RG_bam = "{}/RG.{}.sorted.bam".format(picard_dir,ID)
    params:
        ID = ID,
        species = species,
        tmp = "{}/{}".format(picard_dir,ID)
    conda:
        "envs/gandalf.yaml"
    shell:
        "picard -Xms1024m -Xmx1024m AddOrReplaceReadGroups VALIDATION_STRINGENCY=LENIENT RGLB={params.ID} RGPL=illumina RGPU=gandalf I={input.bam} O={output.RG_bam} SORT_ORDER=coordinate CREATE_INDEX=TRUE TMP_DIR={params.tmp} RGSM={params.ID} ID={params.ID} && rm -r {params.tmp}"

## Rmdup ##
rule gandalf_rmdup:
    input:
        RG_bam = "{}/RG.{}.sorted.bam".format(picard_dir,ID)
    output:
        dup_bam = "{}/dup.{}.sorted.bam".format(samtools_dir,ID)
    conda:
        "envs/gandalf.yaml" 
    shell:
        "samtools rmdup {input.RG_bam} {output.dup_bam}"

## Clean mapping ##
rule gandalf_cleanmapping:
    input:
        dup_bam = "{}/dup.{}.sorted.bam".format(samtools_dir,ID)
    output:
        clean_bam = "{}/min30.{}.sorted.bam".format(samtools_dir,ID)
    conda:
        "envs/gandalf.yaml" 
    shell:
        "samtools view -b -F 0x0100 -f 0x0002 -o {output.clean_bam} -q 30 {input.dup_bam}"

## Unique list ##
rule gandalf_uniquelist:
    input:
        clean_bam = "{}/min30.{}.sorted.bam".format(samtools_dir,ID)
    output:
        unique_list = "{}/min30.{}.lst".format(samtools_dir,ID)
    conda:
        "envs/gandalf.yaml" 
    shell:
        "samtools view {input.clean_bam} | cut -f 1 | sort -T $PWD | uniq -c | grep ' 1 ' || true | cut -f8 -d' ' > {output.unique_list}"

## Remove unique if necessary ##
if os.path.isfile("{}/min30.{}.lst".format(samtools_dir,ID)) and os.path.getsize("{}/min30.{}.lst".format(samtools_dir,ID)) > 0:
    rule gandalf_rmunique:
        input:
            clean_bam = "{}/min30.{}.sorted.bam".format(samtools_dir,ID),
            unique_list = "{}/min30.{}.lst".format(samtools_dir,ID)
        output:
            paired_bam = "{}/paired.min30.{}.sorted.bam".format(picard_dir,ID)
        conda:
            "envs/gandalf.yaml" 
        shell:
            "picard -Xms1024m -Xmx1024m FilterSamReads I={input.clean_bam} FILTER=excludeReadList RLF={input.unique_list} O={output.paired_bam}"
else:
    rule gandalf_rename:
        input:
            clean_bam = "{}/min30.{}.sorted.bam".format(samtools_dir,ID),
        output:
            paired_bam = "{}/paired.min30.{}.sorted.bam".format(picard_dir,ID)
        shell:
            "mv {input.clean_bam} {output.paired_bam}"

## Freebayes ##
rule gandalf_freebayes:
    input:
        paired_bam = "{}/paired.min30.{}.sorted.bam".format(picard_dir,ID),
        ref_genome = genome 
    output:
        vcf = "{}/min30.mono.{}.vcf".format(freebayes_dir,ID)
    conda:
        "envs/gandalf.yaml"
    shell:
        "freebayes --report-monomorphic --ploidy 2 -X -u -f {input.ref_genome} {input.paired_bam} > {output.vcf}"

## Filtering ##
rule gandalf_filtering:
    input:
        vcf = "{}/min30.mono.{}.vcf".format(freebayes_dir,ID),
        TE = TE,
        lowcomplex = lowcomplex
    output:
        filtered_vcf = "{}/{}.filter.vcf".format(filter_dir,ID)
    conda:
        "envs/gandalf.yaml"
    shell:    
        "VCFFiltering.py -f {input.vcf} -b {input.TE} -b {input.lowcomplex} -o {output.filtered_vcf}"

## Compress ##
rule gandalf_compress:
    input:
        filtered_vcf = "{}/{}.filter.vcf".format(filter_dir,ID)
    output:
        bgzip_vcf = "{}/{}.filter.vcf.gz".format(filter_dir,ID)
    conda:
        "envs/gandalf.yaml"
    shell:    
        "bgzip {input.filtered_vcf}"

## Tabix ##
rule gandalf_index:
    input:
        bgzip_vcf = "{}/{}.filter.vcf.gz".format(filter_dir,ID)
    output:
        indexed_vcf = "{}/{}.filter.vcf.gz.tbi".format(filter_dir,ID)
    conda:
        "envs/gandalf.yaml"
    shell:    
        "tabix -p vcf {input.bgzip_vcf}"
